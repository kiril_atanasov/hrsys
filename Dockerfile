PROJECT DOCKER:
FROM java:8
VOLUM /tmp
EXPOSE 12555
ADD target/hrsys.jar hrsys.jar
ENTRYPOINT["java", "-jar", "hrsys.jar"]
