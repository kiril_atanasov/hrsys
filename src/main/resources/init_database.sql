 -- CREATE DATABASE hrsys;

USE hrsys;

-- In case of sql server timezone error please run the following two lines:
 -- SET @@global.time_zone = '+00:00';
 -- SET @@session.time_zone = '+00:00';


CREATE TABLE employee (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `address` varchar(50),
  `country` varchar(50),
  `city` varchar(50),
  `post` varchar(50),
  `code` varchar(50),
  `telephone` varchar(50),
  `salary` decimal,
  PRIMARY KEY (`id`)
);

INSERT INTO employee VALUES (1, "Kiril", "Kirilov", "bul Bulgaria, 109", "Bulgaria", "Sofia", "1000", "SF", "359-88-7123123", 1020);
INSERT INTO employee VALUES (2, "Ivan", "Ivanov", "bul Vitosha, 19", "Bulgaria", "Varna", "1000", "VN", "359-88-8124123", 1004);
INSERT INTO employee VALUES (3, "Georgi", "Georgiev", "ul. Maria Luiza 11", "Bulgaria", "Plovdiv", "1000", "PL", "359-88-1123123", 1005);
INSERT INTO employee VALUES (4, "Stamen", "Stamenov", "ul. gen. Suvorov 14", "Bulgaria", "Burgas", "1000", "BS", "359-88-3123123", 1900);

 -- SELECT * FROM employee;


