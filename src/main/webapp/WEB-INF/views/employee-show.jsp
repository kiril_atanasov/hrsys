<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Show Employee</title>
</head>
<body>
<table>
		<tr>
			<td>ID</td>
			<td>${employee.id}</td>
		</tr>
		<tr>
			<td>First Name</td>
			<td>${employee.firstName}</td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td>${employee.lastName}</td>
		</tr>
		<tr>
			<td>Address</td>
			<td>${employee.address}</td>
		</tr>
		<tr>
			<td>Country</td>
			<td>${employee.country}</td>
		</tr>
		<tr>
			<td>City</td>
			<td>${employee.city}</td>
		</tr>
		<tr>
			<td>Post</td>
			<td>${employee.post}</td>
		</tr>
		<tr>
			<td>Code</td>
			<td>${employee.code}</td>
		</tr>
		<tr>
			<td>Telephone</td>
			<td>${employee.telephone}</td>
		</tr>
		<tr>
			<td>Salary</td>
			<td>${employee.salary}</td>
		</tr>
	</table>
	<a href="showEmployees">show employees</a><br/>


</body>
</html>