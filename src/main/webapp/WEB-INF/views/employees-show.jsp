<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Display Employees</title>
</head>
<body>
	<h1>Employees:</h1>
	<table>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td></td>
			<td></td>
		</tr>
		<c:forEach items="${employees}" var="employee">
		<tr>
			<td>${employee.id}</td>
			<td><a href="showEmployee?id=${employee.id}">${employee.firstName}, ${employee.lastName}</a></td>
			<td><a href="editEmployee?id=${employee.id}">edit</a></td>
			<td><a href="deleteEmployee?id=${employee.id}">delete</a></td>
		</tr>
		</c:forEach>
	</table>
	<br/>

	<a href="showCreateEmployee">add employee</a><br/>
	<a href="showLatestEmployees">show latest employees</a><br/>
	<a href="showEmployeesByName">sort employees by name</a><br/>	


</body>
</html>