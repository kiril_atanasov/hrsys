<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create New Employee</title>
<style>
	.error {color: red}
</style>
</head>
<body>
	<form:form action="processCreate" modelAttribute="employee">
		<form:hidden path="id" />
		
        First Name(*): <form:input path="firstName" />
		<form:errors path="firstName" cssClass="error" />
		<br />
		
        Last Name(*): <form:input path="lastName" />
		<form:errors path="lastName" cssClass="error" />
		<br />
		
		address: <form:input path="address" />
		<form:errors path="address" cssClass="error" />
		<br />
		
		country: <form:input path="country" />
		<form:errors path="country" cssClass="error" />
		<br />
		
		city: <form:input path="city" />
		<form:errors path="city" cssClass="error" />
		<br />
		
		post: <form:input path="post" />
		<form:errors path="post" cssClass="error" />
		<br />
		
		code: <form:input path="code" />
		<form:errors path="code" cssClass="error" />
		<br />
		
		telephone: <form:input path="telephone" />
		<form:errors path="telephone" cssClass="error" />
		<br />
		
		salary: <form:input path="salary" />
		<form:errors path="salary" cssClass="error" />
		<br />

		<input type="submit" value="Subtim" />
	</form:form>

</body>
</html>