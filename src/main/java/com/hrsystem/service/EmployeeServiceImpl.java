package com.hrsystem.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hrsystem.model.Employee;
import com.hrsystem.repository.EmployeeDAO;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;

	@Override
	@Transactional
	public List<Employee> getEmployees(Sort sort) {
		List<Employee> result = new ArrayList<>();
		Iterator<Employee> employees = employeeDAO.findAll(sort).iterator();
		while (employees.hasNext()) {
			result.add(employees.next());
		}
		
		return result;		
	}

	@Override
	@Transactional
	public void saveEmployee(Employee employee) {
		employeeDAO.save(employee);
	}

	@Override
	@Transactional
	public Employee getEmployee(long id) {
		return employeeDAO.findById(id).get();
	}

	@Override
	@Transactional
	public void deleteEmployee(long id) {
		employeeDAO.deleteById(id);
	}

}
