package com.hrsystem.service;

import java.util.List;

import org.springframework.data.domain.Sort;

import com.hrsystem.model.Employee;

public interface EmployeeService {
	public List<Employee> getEmployees(Sort sort);

	public void saveEmployee(Employee employee);

	public Employee getEmployee(long id);

	public void deleteEmployee(long id);
}
