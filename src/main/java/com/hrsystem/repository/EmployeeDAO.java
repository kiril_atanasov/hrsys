package com.hrsystem.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import com.hrsystem.model.Employee;

public interface EmployeeDAO extends CrudRepository<Employee, Long> {
	Iterable<Employee> findAll(Sort sort);
}
