package com.hrsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "first_name")
	@NotNull(message = "first name is required")
	@Size(min = 1, message = "first name must be at least 1 character long")
	private String firstName;

	@Column(name = "last_name")
	@NotNull(message = "last name is required")
	@Size(min = 1, message = "last name must be at least 1 character long")
	private String lastName;
	
	@Column(name = "address")
	@NotNull(message = "address is required")
	private String address;
	
	@Column(name = "country")
	@NotNull(message = "country is required")
	@Size(min = 3, message = "country must be at least 3 characters long")
	private String country;
	
	@Column(name = "city")
	@NotNull(message = "city is required")
	@Size(min = 2, message = "city must be at least 2 characters long")
	private String city;
	
	@Column(name = "post")
	@NotNull(message = "post is required")
	@Pattern(regexp="^[0-9]{4}", message="post - enter 4 digit post code")
	private String post;
	
	@Column(name = "code")
	@NotNull(message = "code is required")
	private String code;
	
	@Column(name = "telephone")
	@NotNull(message = "telephone is required")
	@Size(min = 6, message = "telephone must be at least 6 symbols long")
	@Size(max = 30, message = "telephone must be less than 30 symbols long")
	private String telephone;
	
	@NotNull(message = "salary is required")
	@Min(value=500, message="salary must be greater or equal to 500")
	@Column(name = "salary")
	private double salary;

	public Employee() {
	}

	public Employee(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Employee(@NotNull(message = "is required") @Size(min = 1, message = "is required") String firstName,
			@NotNull(message = "is required") @Size(min = 1, message = "is required") String lastName, String address,
			String country, String city, String post, String code, String telephone, double salary) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.country = country;
		this.city = city;
		this.post = post;
		this.code = code;
		this.telephone = telephone;
		this.salary = salary;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
				+ ", country=" + country + ", city=" + city + ", post=" + post + ", code=" + code + ", telephone="
				+ telephone + ", salary=" + salary + "]";
	}
	
	
}
