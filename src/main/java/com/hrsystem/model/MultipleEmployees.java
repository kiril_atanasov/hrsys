package com.hrsystem.model;

import java.util.List;

public class MultipleEmployees {
	private List<Long> ids;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public MultipleEmployees() {
		
	}
}
