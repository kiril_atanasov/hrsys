package com.hrsystem.utils;

import org.springframework.data.domain.Sort;

public class DisplayCommands {

	private static final String SORT_NAME_INPUT = "name";
	
	private static final String SORT_LATEST_INPUT= "latest";
	
	private static final Sort DEFAULT_SORT = Sort.by("id");

	public static Sort getSortType(String parameter) {
		// special cases
		switch (parameter) {
		case SORT_LATEST_INPUT:
			return Sort.by("id").descending();
			
		case SORT_NAME_INPUT:
			return Sort.by("firstName", "lastName");
		}
		
		if ( parameter != null && !parameter.isEmpty()) {
			return Sort.by(parameter);
		}
		
		return DEFAULT_SORT;
	}

}
