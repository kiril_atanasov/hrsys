package com.hrsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.model.Employee;
import com.hrsystem.model.MultipleEmployees;
import com.hrsystem.service.EmployeeService;
import com.hrsystem.utils.DisplayCommands;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping
	public ResponseEntity<List<Employee>> showAllEmployees(
			@RequestParam(value = "sorttype", defaultValue = "id") String sortType) {
		Sort sort = DisplayCommands.getSortType(sortType);
		List<Employee> employees = employeeService.getEmployees(sort);
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Employee> showEmployee(@PathVariable("id") long id) {
		Employee employee = employeeService.getEmployee(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> saveEmployee(@Valid @RequestBody Employee employee) {
		employeeService.saveEmployee(employee);
		HttpHeaders responseHeader = new HttpHeaders();
		return new ResponseEntity<>(employee, responseHeader, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<?> updateEmployee(@Valid @RequestBody Employee employee) {
		employeeService.saveEmployee(employee);
		HttpHeaders responseHeader = new HttpHeaders();
		return new ResponseEntity<>(employee, responseHeader, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") long id) {
		employeeService.deleteEmployee(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/multiple")
	public ResponseEntity<?> deleteEmployee(@RequestBody MultipleEmployees multipleEmployees) {
		for (long id : multipleEmployees.getIds()) {
			employeeService.deleteEmployee(id);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

}