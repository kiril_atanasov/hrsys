# Project Name
hrsys

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)

## General info
Java Web Application with CRUD operations


## Technologies
* spring MVC
* hibernate
* mysql
* maven

## Setup
Go to the hibernate.properties file and add
    jdbc.username = xxx
    jdbc.password = xxx
After connecting to your database you can use the file init_database.sql to initialize the database.

## Features
List of features ready
* add employee
* edit employee
* delete employee
* fetch latest employees
* sort by name in ascending order
* display details
* junit tests

Missing
* Delete multiple employees / more than one employee.
* Docker
